import { TodoData } from './../../src/todoData';
import {StageComponent} from 'aurelia-testing';
import {bootstrap} from 'aurelia-bootstrapper';

describe('TaskDetailComponent', () => {
  let component;

  beforeEach(() => {
    component = StageComponent
      .withResources('task-detail')
      .inView('<task-detail todo.bind="todo"></task-detail>')
      .boundTo({ todo: new TodoData("test task") });
  });

  it('should render task', done => {
    component.create(bootstrap).then(() => {
      const taskElement = document.querySelector('.task');
      expect(taskElement.innerHTML).toBe('test task');
      done();
    }).catch(e => { console.log(e.toString()) });
  });

  afterEach(() => {
    component.dispose();
  });
});
