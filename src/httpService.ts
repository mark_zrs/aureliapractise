import { lazy } from 'aurelia-framework';
import { HttpClient } from 'aurelia-fetch-client';

export class HttpService {
  static async  getResponse<T>(baseUrl: string, url: string): Promise<T> {
    let client: HttpClient = new HttpClient();
    client.configure(config => {
      config
        .useStandardConfiguration()
        .withBaseUrl(baseUrl);
    });
    const response = await client.fetch(url);
    return await response.json();
  }
}
