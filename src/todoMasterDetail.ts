import { HttpService } from './httpService';
import { TodoData } from './todoData';

export class TodoMasterDetail {
  tasks: TodoData[];
  selectedTask: TodoData;

  activate() {
    // this.tasks = await HttpService.getResponse<TodoData[]>("http://localhost:62290/api/", "todo")
    this.tasks = new Array<TodoData>();
    this.tasks.push(new TodoData("task 1"));
    this.tasks.push(new TodoData("task 2"));
    this.tasks.push(new TodoData("task 3"));

  }
  click(task: TodoData) {
    if (this.selectedTask != task) {
      this.selectedTask = task;
    } else {
      this.selectedTask = null;
    }
  }
}
