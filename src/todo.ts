import { TodoData } from './todoData'
export class Todo {
    tasks: TodoData[] = [];
    taskDesc: string = "";

    addTodo() {
        if (this.taskDesc) {
            console.log(this.taskDesc);
            this.tasks.push(new TodoData(this.taskDesc));
            this.taskDesc = "";
            console.log(this.tasks);
        }
    }
    removeTodo(todo: TodoData) {
        let index = this.tasks.indexOf(todo);
        if (index !== -1) {
            this.tasks.splice(index, 1);
        }
    }
}