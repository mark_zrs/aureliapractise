import { TodoData } from './todoData';
import { bindable } from "aurelia-framework";

export class TaskDetailCustomElement {

  @bindable todo: TodoData;

}
